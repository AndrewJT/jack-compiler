#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <filesystem>
#include "token.h"
#include "parser.h"

std::vector <Subroutine> stillToGet;

bool alreadyToGet(Subroutine s){

    std::vector<Subroutine>::iterator itr;
    for(itr = stillToGet.begin(); itr != stillToGet.end(); ++itr) {
        if(*itr == s){
        return true;
        }
    }
    return false;

}

void compFile(std::string fileName) {

    Parser p = Parser(fileName);

    p.StartParse();

    std::cout << fileName << " parsed succesfully!" << std::endl;

    std::vector<Subroutine>::iterator itr;

    for(itr = p.toBeFound.begin(); itr != p.toBeFound.end(); ++itr) {

        if (!alreadyToGet(*itr)){
            stillToGet.push_back(*itr);
        }
    }
}

int main(int argc, char *argv[]) {

    std::vector<std::string> args(argv, argv + argc);

    std::string path = args[1];

    if(!std::filesystem::is_directory(path)){
        std::cout << path << " is not a directory";
        exit(0);
    }

    std::cout << "Starting\n:" << path << std::endl;
    
    for (const auto & entry : std::filesystem::directory_iterator(path)){
        std::string filePath = entry.path().string();
        if(filePath.substr(filePath.find_last_of(".") + 1) == "jack"){
            //This is a jack file. Compile it
            std::cout << "\nFound " << filePath << " compiling...";
            compFile(filePath);
            std::cout << "Done \n";
        }
    }

    std::vector<Subroutine>::iterator itr;

    std::cout << "\nCOMPILE SUCCESS\n";

    return 0;
}
