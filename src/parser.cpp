#include "parser.h"
#include "token.h"
#include "symbollist.h"
#include "symbol.h"
#include "vmwriter.h"
#include <string>
#include <iostream>
#include <algorithm>

//A paser

void Parser::classDeclar(){

    labelCounter = 0;

    Token nxt = GetNextToken();

    if(nxt.Type == Token::keyword && nxt.Lexeme == "class"){

        nxt = GetNextToken();
        if(nxt.Type == Token::id){

            //Add the class ID to the program table
            CurrentClassName = nxt.Lexeme;
            
            nxt = GetNextToken();
            if(nxt.Type == Token::symbol && nxt.Lexeme == "{") {

                //We are in a new scope and so need a new table
                SymbolList classTable = SymbolList(NULL);
                currentTable = &classTable;
                knownClasses.push_back(currentTable);

                //Zero or more memberDeclair
                nxt = PeekNextToken();
                //If the next symbol isnt } its a member
                while(!(nxt.Type == Token::symbol && nxt.Lexeme == "}")){

                    //If there is EOF befor } its an error
                    if(nxt.Type == Token::eof){
                        error("Unexpected EOF. Expecting } at the end of class body");
                    }

                    //Otherwise its a member
                    memberDeclar();
                    nxt = PeekNextToken();
                }

                //classTable.debugPrint(1);

            } else {
                error("Expecting {");
            }

            

        } else {
            error("Expecting class name identifier!");
        }
    } else {
        error("Expecting 'class'!");
    }

}

void Parser::memberDeclar(){

    Token nxt = PeekNextToken();

    if((nxt.Type == Token::keyword) && (nxt.Lexeme == "static" || nxt.Lexeme == "field")){
        //This is a var
        classVarDeclar();
    } else if((nxt.Type == Token::keyword) && (nxt.Lexeme == "constructor" || nxt.Lexeme == "function"  || nxt.Lexeme == "method")){
        //This is a subroutine
        subroutineDeclar();
    } else {
        error("Expecting a subroutine or variable declaration!");
    }
}

void Parser::classVarDeclar(){
    Token nxt = GetNextToken();

    if((nxt.Type == Token::keyword) && (nxt.Lexeme == "static" || nxt.Lexeme == "field")){

        std::string kind = nxt.Lexeme;
        nxt = PeekNextToken();    
        std::string varType = nxt.Lexeme;    

        type();
        nxt = GetNextToken();
        if (nxt.Type == Token::id){
            //Add token symbol to table
            currentTable->AddSymbol(nxt.Lexeme, varType, Symbol::KindFromString(kind));
            
            //Now allow for many more IDs seperated by commas
            nxt = PeekNextToken();

            while(nxt.Type == Token::symbol && nxt.Lexeme == ","){
                //There are more of these
                GetNextToken();     //Eat the comma
                nxt = GetNextToken();

                if(nxt.Type != Token::id){
                    error("Expecting another variable identifier after comma!");
                } else {
                    //Add the next id
                    currentTable->AddSymbol(nxt.Lexeme, varType, Symbol::KindFromString(kind));
                }
                nxt = PeekNextToken();
            }

        } else {
            error("Expecting variable identifier!");
        }

        nxt = GetNextToken();
        if(nxt.Lexeme != ";"){
            error("Expecting ; after variable declaration");
        }

    } else {
        error("Expecting 'static' or 'field' ");
    }
}
void Parser::type(){
    Token nxt = GetNextToken();

    if(nxt.Type == Token::keyword){
        if(nxt.Lexeme != "int" && nxt.Lexeme != "char" && nxt.Lexeme != "boolean"){
            error("Invalid type or type identifier!");
        }
    } else if (nxt.Type != Token::id){
        //TODO: Implement possible classes as types
        error("Invalid type or type identifier!");
    }
}

void Parser::subroutineDeclar(){

    Token nxt = GetNextToken();

    if((nxt.Type == Token::keyword) && (nxt.Lexeme == "constructor" || nxt.Lexeme == "function"  || nxt.Lexeme == "method")){
        
        //Check the RETURN type
        std::string subType = nxt.Lexeme;   //Constructor method or function etc...

        nxt = PeekNextToken();  //This is the type
        currentSubRetType = nxt.Lexeme;   //What does this return

        if(subType == "constructor"){
            //If this is a constructor the ret type must be the same as current class

            GetNextToken(); 

            if(currentSubRetType != CurrentClassName){
                error("Constructor must be of type " + CurrentClassName);
            }

            currentSubRetType = CurrentClassName;

        } else if(currentSubRetType == "void"){
           //The next token is void. Eat it
           GetNextToken(); 
        } else {
            //It must be a type
            currentSubRetType = PeekNextToken().Lexeme;
            type();
        }
        
        //Check the RETURN type is followed by an ID
        nxt = GetNextToken();

        if(nxt.Type == Token::id){

            std::string subName = nxt.Lexeme;

            //Setup symbol table            
            Subroutine newSubroutine = Subroutine(subName, CurrentClassName, currentSubRetType, Subroutine::UNKNOWN);
            SymbolList subRoutineTable = SymbolList(currentTable);
            if(subType == "method"){
                //This subroutine needs to hold the object
                newSubroutine.type = Subroutine::METHOD;
                subRoutineTable.AddSymbol("this", CurrentClassName, Symbol::ARGUMENT, true);
            } else if(subType == "function"){
                newSubroutine.type = Subroutine::FUNCTION;
            } else if(subType == "constructor"){
                newSubroutine.type = Subroutine::CONSTRUCTOR;
            } else {
                error("Invalid subroutine type. Expecting method, function or constructor");
            }

            currentTable = &subRoutineTable;

            nxt = GetNextToken();

            if(nxt.Type == Token::symbol && nxt.Lexeme == "("){

                //Add subroutine to the classes list
                workingList.clear();
                paramList();

                for(std::string const param: workingList){
                    newSubroutine.addType(param);
                }
                
                nxt = GetNextToken();
                if(nxt.Type == Token::symbol && nxt.Lexeme == ")"){

                    std::streampos initialPos = output.output.tellp();

                    output.writeFunction(CurrentClassName + "." + subName, 0);

                    if(newSubroutine.type == Subroutine::CONSTRUCTOR){
                        //Do the setup for constructor
                        //Get number of fields
                        output.writePush(VmWriter::CONST, currentTable->parent->numberOfFields());
                        output.writeCall("Memory.alloc", 1);
                        
                        //Set this pointer
                        output.writePop(VmWriter::POINTER, 0);
                    } else if (newSubroutine.type == Subroutine::METHOD){
                        //Set this pointer
                        output.writePush(VmWriter::ARG, 0);
                        output.writePop(VmWriter::POINTER, 0);
                    }

                    subRoutines.push_back(newSubroutine);
                    subroutineBody();

                    std::streampos postPos = output.output.tellp();

                    output.output.seekp(initialPos);
                    output.writeFunction(CurrentClassName + "." + subName, currentTable->numberOfLocals());

                    output.output.seekp(postPos);                    

                    currentTable = currentTable->parent;

                } else {
                    error("Expecting )");
                }

            } else {
                error("Expecting (");
            }

        } else {
            error("Expecting subroutine identifier");
        }

    } else {
        error("Expecting subroutine type keyword");
    }


}
void Parser::paramList(){

    Token nxt = PeekNextToken();

    //Do nothing if the paramlist is empty
    if(!(nxt.Type == Token::symbol && nxt.Lexeme == ")")){
        //List is not empty
        nxt = PeekNextToken();
        std::string paramType = nxt.Lexeme;
        type();
        workingList.push_back(paramType);
        nxt = GetNextToken();
        if(nxt.Type == Token::id){

            std::string paramName = nxt.Lexeme;
            currentTable->AddSymbol(paramName, paramType, Symbol::ARGUMENT, true);

            nxt = PeekNextToken();
            
            //Similar to as done abive somewhere
            while(nxt.Type == Token::symbol && nxt.Lexeme == ","){
                GetNextToken(); //Eat the ,
                paramType = PeekNextToken().Lexeme;
                type();
                workingList.push_back(paramType);
                nxt = GetNextToken();
                if(nxt.Type != Token::id){
                    error("Expecting a parameter identifier");
                } else {
                    currentTable->AddSymbol(nxt.Lexeme, paramType, Symbol::ARGUMENT, true);
                }
                nxt = PeekNextToken();
            }
            
        } else {
            error("Expecting a parameter identifier");
        }
    }
}

void Parser::subroutineBody(){

    Token nxt = GetNextToken();

    if(nxt.Type == Token::symbol && nxt.Lexeme == "{"){

        //Check for many statements
        Token nxt = PeekNextToken();
        while(nxt.Type == Token::keyword && (nxt.Lexeme == "var" || nxt.Lexeme == "let" || nxt.Lexeme == "if" || nxt.Lexeme == "while" || nxt.Lexeme == "do" || nxt.Lexeme == "return")){
            statement();
            nxt = PeekNextToken();
        }

        nxt = GetNextToken();

        if(!(nxt.Type == Token::symbol && nxt.Lexeme == "}")){
            error("Expecting } at end of subroutine definition");
        }

    } else {
        error("Expecting {");
    }

}

void Parser::statement(){

    Token nxt = PeekNextToken();

    if(nxt.Type == Token::keyword && nxt.Lexeme == "var"){
        varDeclarStatement();
    } else if (nxt.Type == Token::keyword && nxt.Lexeme == "let"){
        letStatemnt();
    } else if (nxt.Type == Token::keyword && nxt.Lexeme == "if"){
        ifStatement();
    } else if (nxt.Type == Token::keyword && nxt.Lexeme == "while"){
        whileStatement();
    } else if (nxt.Type == Token::keyword && nxt.Lexeme == "do"){
        doStatement();
    } else if (nxt.Type == Token::keyword && nxt.Lexeme == "return"){
        returnStatemnt();
        output.writeReturn();
    } else {
        error("Expecting statement");
    }
}

void Parser::varDeclarStatement(){
    Token nxt = GetNextToken();
    
    if(nxt.Type == Token::keyword && nxt.Lexeme == "var"){

        std::string varType = PeekNextToken().Lexeme;

        type();

        nxt = GetNextToken();

        if(nxt.Type == Token::id){
            currentTable->AddSymbol(nxt.Lexeme, varType, Symbol::VAR);

            //Similar to as done abive somewhere
            nxt = PeekNextToken();
            while(nxt.Type == Token::symbol && nxt.Lexeme == ","){
                GetNextToken(); //Eat the ,
                nxt = GetNextToken();
                if(nxt.Type != Token::id){
                    error("Expecting a parameter identifier");
                } else {
                    currentTable->AddSymbol(nxt.Lexeme, varType, Symbol::VAR);
                }
                nxt = PeekNextToken();
            }

            nxt = GetNextToken();

            if(!(nxt.Type == Token::symbol && nxt.Lexeme == ";")){
                error("Expecting ; ");
            }
        } else {
            error("Expecting identifier");
        }

    } else {
        error("Expecting var");
    }
}

void Parser::letStatemnt(){

    Token nxt = GetNextToken();
    
    if(nxt.Type == Token::keyword && nxt.Lexeme == "let"){

        nxt = GetNextToken();
        if(nxt.Type == Token::id){

            Symbol *symBolRef = currentTable->isInScope(nxt.Lexeme);
            if(symBolRef == NULL){
                //Variable not declared
                error("Variable " + nxt.Lexeme + " has not been declared in the scope!");
            } else {
                symBolRef->Initialised = true;
            }

            output.debugMsg("Letting " + symBolRef->Name);

            nxt = GetNextToken();
            //Allow for [expression]
            if(nxt.Type == Token::symbol && nxt.Lexeme=="["){
                
                //Expect an int as array list indices must be ints
                expressionType = "int";
                output.debugMsg("There should be an int noe");
                expression();   //This should push a int into the stack

                pushVar(symBolRef);
                output.writeArithmetic(VmWriter::ADD);
                nxt = GetNextToken();

                if(!(nxt.Type == Token::symbol && nxt.Lexeme=="]")){
                    error("Expecting ]");
                }

                nxt = GetNextToken();

                if(nxt.Type == Token::symbol && nxt.Lexeme=="="){
                    expressionType = symBolRef->Type;
                    unInitialisedFlag = false;
                    expression();   //This adds a value to be assigned to the stack

                    //Put the expression result into temp
                    output.writePop(VmWriter::TEMP, 0);

                    //Sets THAT to the index
                    output.writePop(VmWriter::POINTER, 1);

                    //Pushes expression resut and writes it
                    output.writePush(VmWriter::TEMP, 0);
                    output.writePop(VmWriter::THAT, 0);

                } else {
                    error("Expecting =");
                }             
            } else {

                //This is a local field

                if(nxt.Type == Token::symbol && nxt.Lexeme=="="){
                    expressionType = symBolRef->Type;
                    unInitialisedFlag = false;
                    expression();

                    if(unInitialisedFlag){
                        error("Trying to let variable be an uninitialised var!");
                    }
                    unInitialisedFlag = false;

                    //pop the top of stack variable to wherever this is stored
                    popVar(symBolRef);

                } else {
                    error("Expecting =");
                }
            }

        } else {
            error("Expecting identifier");
        }

        nxt = GetNextToken();
        if(nxt.Lexeme != ";"){
            error("Expecting ; after let statement!");
        }

    } else {

        error("Expecting Let statement");

    }

    output.debugMsg("End let");

}

void Parser::ifStatement(){

    Token nxt = GetNextToken();
    
    if(nxt.Type == Token::keyword && nxt.Lexeme == "if"){

        std::string ifTrue = "iftrue" + std::to_string(labelCounter);
        std::string ifFalse = "iffalse" + std::to_string(labelCounter);
        std::string endIf = "end" + std::to_string(labelCounter);
        labelCounter++;

        Token nxt = GetNextToken();
    
        if(nxt.Type == Token::symbol && nxt.Lexeme == "("){
            expressionType = "boolean";
            expression();

            output.writeIf(ifTrue);
            output.writeGoto(ifFalse);
            output.writeLabel(ifTrue);

            nxt = GetNextToken();

            if(nxt.Type == Token::symbol && nxt.Lexeme == ")"){
                nxt = GetNextToken();
                if(nxt.Type == Token::symbol && nxt.Lexeme == "{"){

                    //Check for many statements
                    Token nxt = PeekNextToken();
                    while(nxt.Type == Token::keyword && (nxt.Lexeme == "let" || nxt.Lexeme == "if" || nxt.Lexeme == "while" || nxt.Lexeme == "do" || nxt.Lexeme == "return")){                      
                        statement();
                        nxt = PeekNextToken();
                    }
                    nxt = GetNextToken();

                    if(!(nxt.Type == Token::symbol && nxt.Lexeme == "}")){
                        error("Expecting } at end of if body");
                    }

                    nxt = PeekNextToken();

                    if(nxt.Type == Token::keyword && nxt.Lexeme == "else"){

                        output.writeGoto(endIf);
                        output.writeLabel(ifFalse);

                        //There is an else statement
                        GetNextToken();     //Eat else
                        nxt = GetNextToken();

                        if(nxt.Type == Token::symbol && nxt.Lexeme == "{"){

                            //Check for many statements
                            Token nxt = PeekNextToken();
                            while(nxt.Lexeme != "}"){
                                statement();
                                nxt = PeekNextToken();
                            }

                            nxt = GetNextToken();
                            if(!(nxt.Type == Token::symbol && nxt.Lexeme == "}")){
                                error("Expecting } at end of else body");
                            }

                        } else {
                            error("Expecting {");
                        }

                        output.writeLabel(endIf);

                    } else {
                        output.writeLabel(ifFalse);
                    }
                    
                } else {
                    error("Expecting {");
                }

            } else {
                error("Expecting )");
            }

        } else {
            error("Expecting (");
        }

        
    } else {
        error("Expecting if");
    }

}
void Parser::whileStatement(){

    Token nxt = GetNextToken();
    
    if(nxt.Type == Token::keyword && nxt.Lexeme == "while"){

        std::string start = "startLoop" + std::to_string(labelCounter);
        std::string end = "endLoop" + std::to_string(labelCounter);
        labelCounter++;

        output.writeLabel(start);

        Token nxt = GetNextToken();
    
        if(nxt.Type == Token::symbol && nxt.Lexeme == "("){

            expressionType = "boolean";
            expression();

            output.writeArithmetic(VmWriter::NOT);
            output.writeIf(end);

            nxt = GetNextToken();

            if(nxt.Type == Token::symbol && nxt.Lexeme == ")"){
                nxt = GetNextToken();
                if(nxt.Type == Token::symbol && nxt.Lexeme == "{"){

                    //Check for many statements
                    Token nxt = PeekNextToken();
                    while(nxt.Lexeme != "}"){
                        
                        statement();
                        nxt = PeekNextToken();
                    }

                    nxt = GetNextToken();

                    if(!(nxt.Type == Token::symbol && nxt.Lexeme == "}")){
                        error("Expecting } at end of while body");
                    }

                    output.writeGoto(start);
                    output.writeLabel(end);

                } else {
                    error("Expecting {");
                }

            } else {
                error("Expecting )");
            }

        } else {
            error("Expecting (");
        }

        
    } else {
        error("Expecting while");
    }
}

void Parser::doStatement(){

    Token nxt = GetNextToken();
    
    if(nxt.Type == Token::keyword && nxt.Lexeme == "do"){

        subroutineCall();

        nxt = GetNextToken();
        if(!(nxt.Type == Token::symbol && nxt.Lexeme == ";")){
            error("Expecting ;");
        }  

        //Discard the result
        output.writePop(VmWriter::TEMP, 0);

    } else {
        error("Expecting do");
    }
}

void Parser::subroutineCall(){

    Token nxt = GetNextToken();
    if(nxt.Type == Token::id){

        std::string firstName = nxt.Lexeme;
        std::string subName = firstName;
        std::string className = CurrentClassName;
        Symbol *object = NULL;


        Subroutine::SubType subroutineType = Subroutine::METHOD;

        nxt = PeekNextToken();
        if(nxt.Type == Token::symbol && nxt.Lexeme == "."){
            //There is a further identifier
            GetNextToken();     //Consume the .
            nxt = GetNextToken();
            if(nxt.Type != Token::id){
                error("Expected identifier after the .");
            }

            //Find out if this is a method still
            object = currentTable->isInScope(firstName);

            subName = nxt.Lexeme;

            if(object == NULL){
                //This must be a class function
                className = firstName;
                subroutineType = Subroutine::FUNCTION;
            } else {
                //This is a method of an object
                //We must push the orbect to the the stack
                className = object->Type;
            }            
        }

        nxt = GetNextToken();

        if(nxt.Type == Token::symbol && nxt.Lexeme == "("){

            if(subroutineType == Subroutine::METHOD){
                if(object == NULL){
                    output.writePush(VmWriter::POINTER, 0);
                } else {
                    pushVar(object);
                }
            }

            expressionType = "*";
            //Get number of parameters
            int tempExpressionNo = expressionNo;
            expressionNo = 0;
            expressionList();
            int numParams = expressionNo;
            expressionNo = tempExpressionNo;

            doSubRoutine(className, subName, expressionType, numParams, subroutineType);

            nxt = GetNextToken();
            if(!(nxt.Type == Token::symbol && nxt.Lexeme == ")")){
                error("Expecting ) after subroutine call");
            }
        }

    } else {
        error("Expecting subroutine identifier!");
    }
}

void Parser::expressionList(){

    int count = 0;

    std::string oldType = expressionType;
    expressionType = "*";

    Token nxt = PeekNextToken();

    if(isExpression(nxt)) {
        //There is probably an expression
        expression();
        count++;

        nxt = PeekNextToken();
        while(nxt.Type == Token::symbol && nxt.Lexeme == ","){

            GetNextToken();     //Eat comma
            expression();

            count ++;

            nxt = PeekNextToken();
        }
    }

    expressionType = oldType;
    expressionNo = count;
}

void Parser::returnStatemnt(){
    Token nxt = GetNextToken();
    
    if(nxt.Type == Token::keyword && nxt.Lexeme == "return"){

        nxt = PeekNextToken();
        if(isExpression(nxt)){
            expressionType = currentSubRetType;
            expression();
        } else {
            //This is probs a void func. Push zero
            output.writePush(VmWriter::CONST, 0);
        }

        nxt = GetNextToken();

        if(!(nxt.Type == Token::symbol && nxt.Lexeme == ";")){
            error("Expecting ;");
        }
    } else {
        error("Expecting return!");
    }
}

void Parser::expression(){

    relationalExpression();

    Token nxt = PeekNextToken();

    while(nxt.Type == Token::symbol && (nxt.Lexeme == "&" || nxt.Lexeme == "|")){

        //The type returned from this can only be a boolean or a bit shifted op
        if(!(expressionType == "int" || expressionType == "char" || expressionType == "boolean") && expressionType != "*"){
            error("Expecting type " + expressionType + " got a boolean or bit-shift op instead!");
        }

        GetNextToken();     //Consume
        relationalExpression();

        //Do the operation
        if(nxt.Lexeme == "&"){
            output.writeArithmetic(VmWriter::AND);
        } else if(nxt.Lexeme == "|"){
            output.writeArithmetic(VmWriter::OR);
        }

        nxt = PeekNextToken();
    }
}

void Parser::relationalExpression(){
    
    ArithmeticExpression();

    Token nxt = PeekNextToken();

    while(nxt.Type == Token::symbol && (nxt.Lexeme == "=" || nxt.Lexeme == ">" || nxt.Lexeme == "<")){

        //This can only preoduce a boolean output
        if(expressionType != "boolean" && expressionType != "*"){
            error("Expecting " + expressionType + " got boolean from relational expression!");
        }
        GetNextToken();

        ArithmeticExpression();

        //Do the operation
        if(nxt.Lexeme == "="){
            output.writeArithmetic(VmWriter::EQ);
        } else if(nxt.Lexeme == ">"){
            output.writeArithmetic(VmWriter::GT);
        } else if(nxt.Lexeme == "<"){
            output.writeArithmetic(VmWriter::LT);
        }

        nxt = PeekNextToken();
    }
}

void Parser::ArithmeticExpression(){

    term();

    Token nxt = PeekNextToken();

    while(nxt.Type == Token::symbol && (nxt.Lexeme == "+" || nxt.Lexeme == "-")){

        //Adding can only happen on integers
        if(expressionType != "int" && expressionType != "*"){
            error("Expecting " + expressionType + " got int from arithmetic expression!");
        }
        workingType = "int";
        GetNextToken();

        term();

        if(nxt.Lexeme == "+"){
            output.writeArithmetic(VmWriter::ADD);
        } else if(nxt.Lexeme == "-"){
            output.writeArithmetic(VmWriter::SUB);
        }

        nxt = PeekNextToken();

    }
}

void Parser::term(){

    factor();

    Token nxt = PeekNextToken();

    while(nxt.Type == Token::symbol && (nxt.Lexeme == "*" || nxt.Lexeme == "/")){

        //Adding can only happen on integers
        if(expressionType != "int" && expressionType != "*"){
            error("Expecting " + expressionType + " got int from term!");
        }

        GetNextToken();

        factor();

        if(nxt.Lexeme == "*"){
            output.writeCall("Math.multiply", 2);
        } else if(nxt.Lexeme == "/"){
            output.writeCall("Math.divide", 2);
        }

        nxt = PeekNextToken();

    } 
}

void Parser::factor(){

    Token nxt = PeekNextToken();

    std::string precedingOp = nxt.Lexeme;

    if(nxt.Type == Token::symbol && (nxt.Lexeme == "-" || nxt.Lexeme == "~")){

        if(nxt.Lexeme == "-"){
            //arithmetic negate. Must be int
            //Adding can only happen on integers
            if(expressionType != "int" && expressionType != "*"){
                error("Expecting " + expressionType + " got int from factor!");
            }
            //output.writeArithmetic(VmWriter::NEG);
        }

        GetNextToken();     //If factor starts with - or ~ consume it
        nxt = PeekNextToken();
    }

    if(isOperand(nxt)){
        classRef = "";  //Reset this so it can be used in obj.Field
        //std::string oldType = expressionType;
        expressionType = "*";
        operand();
        //expressionType = oldType;
    } else {
        error("Expecting valid factor!");
    }

    //Apply the negation after
    if(precedingOp == "-"){
        output.writeArithmetic(VmWriter::NEG);
    }else if(precedingOp == "~"){
        output.writeArithmetic(VmWriter::NOT);
    }    
}

void Parser::operand(){

    //There can be lots of type checking here

    Token nxt = PeekNextToken();
    if(nxt.Type == Token::num){

        if(expressionType != "int" && expressionType != "*"){
            error("Expecting type " + expressionType + " got int instead");
        }

        output.writePush(VmWriter::CONST, std::stoi(nxt.Lexeme));

        GetNextToken();

    } else if (nxt.Type == Token::id){

        std::string firstVarName = nxt.Lexeme;
        std::string opVarName = firstVarName; 

        GetNextToken();
        nxt = PeekNextToken();

        if(nxt.Type == Token::symbol && nxt.Lexeme == "."){

            GetNextToken();     //Consume the .

            nxt = GetNextToken();

            if(nxt.Type == Token::id){
                
                //If this is set we are using this insteads
                opVarName = nxt.Lexeme;

                nxt = PeekNextToken();

                //Doing operations on firstVarName.opVarName
                if(nxt.Type == Token::symbol && nxt.Lexeme == "["){

                    GetNextToken();
                    //This is a array index
                    //I dont think pointing to static or member arrays in other Classes is allowed
                    error("Reference to member arrays not implemented");

                } else if(nxt.Type == Token::symbol && nxt.Lexeme == "("){

                    GetNextToken();

                    //Get number of parameters
                    workingList.clear();
                    int tempExpressionNo = expressionNo;
                    expressionNo = 0;

                    //

                    //Calling this will add all the expressions to the stack

                    Symbol *var = currentTable->isInScope(firstVarName);
                    if(var != NULL){
                        pushVar(var);
                    }

                    expressionList();
                    int numParams = expressionNo;
                    expressionNo = tempExpressionNo;

                    //Is the firstVarname a known object?
                    //currentTable->debugPrint(5);
                    if(var == NULL){
                        //The first string is a class
                        doSubRoutine(firstVarName, opVarName, expressionType, numParams, Subroutine::FUNCTION);
                    } else {
                        //The first string is an object
                        doSubRoutine(var->Type, opVarName, expressionType, numParams, Subroutine::METHOD);
                    }
                    
                    nxt = GetNextToken();

                    if(nxt.Lexeme != ")"){
                        error("Expecting ) after list of parameters");
                    }
                } else {
                    //This should be a static variable of a class
                    error("Other class static var not implemented!");
                    //lookForSymbol(firstVarName + "." + opVarName, expressionType, Symbol::STATIC);
                }

            } else {
                error("Expecting identifier after .");
            }
        } else {
            //Doing operations on opVarName
            if(nxt.Type == Token::symbol && nxt.Lexeme == "["){

                GetNextToken();
                //This is a array index
                Symbol *symRef = currentTable->isInScope(opVarName);

                if(symRef == NULL){
                    error("Undefined array " + opVarName);
                }

                if(symRef->Type != "Array"){
                    error("Expecting valid array identifier befor [");
                }

                //We ant an int as array index
                std::string prevExpType = expressionType;
                expressionType = "int";
                expression();
                expressionType = prevExpType;   //Reset to what was before

                nxt = GetNextToken();
                if(nxt.Lexeme != "]"){
                    error("Expecting ] after array index");
                }

                pushVar(symRef);

                //Add the base and index
                output.writeArithmetic(VmWriter::ADD);
                output.writePop(VmWriter::POINTER, 1);  //Set THAT to the thing we r looking for

                output.writePush(VmWriter::THAT, 0);

            } else if(nxt.Type == Token::symbol && nxt.Lexeme == "("){

                //thing()
                //Must be a local method or function
                GetNextToken();
                //This is an list of expressions to be used as method or function args
                //Get number of parameters

                output.writePush(VmWriter::POINTER, 0);
                workingList.clear();
                int tempExpressionNo = expressionNo;
                expressionNo = 0;
                expressionList();
                int numParams = expressionNo;
                expressionNo = tempExpressionNo;

                doSubRoutine(CurrentClassName, opVarName, expressionType, numParams, Subroutine::METHOD);               

                nxt = GetNextToken();
                if(nxt.Lexeme != ")"){
                    error("Expecting ) after list of parameters");
                }
            } else {
                //This is just a stand alone variable
                //Should be referencing a localy avaliable variable
                Symbol *symRef = currentTable->isInScope(opVarName);

                if(symRef == NULL){
                    error("Variable " + opVarName + " not declaired");
                }

                if(!symRef->Initialised){
                    unInitialisedFlag = true;
                }

                if(symRef->Type != expressionType && expressionType != "*"){
                    error("Expecting " + expressionType + " got " + symRef->Type);
                }

                pushVar(symRef);
            }
        }

    } else if (nxt.Type == Token::string_literal) {

        if("string" != expressionType && expressionType != "*"){
            error("Expecting " + expressionType + " got string");
        }

        constructString(nxt.Lexeme);

        GetNextToken();
        
    } else if (nxt.Type == Token::keyword) {

        if(nxt.Lexeme == "true" || nxt.Lexeme == "false"){

            if(expressionType != "boolean" && expressionType != "*"){
                error("Expecting " + expressionType + " got boolean");
            }

            if(nxt.Lexeme == "true"){
                output.writePush(VmWriter::CONST, 1);
                output.writeArithmetic(VmWriter::NEG);
            } else if(nxt.Lexeme == "false") {
                output.writePush(VmWriter::CONST, 0);
            }

            GetNextToken();

        }else if (nxt.Lexeme == "this"){

            GetNextToken();
            output.writePush(VmWriter::POINTER, 0);

        } else if (nxt.Lexeme == "null"){

            GetNextToken();
            output.writePush(VmWriter::CONST, 0);

        } else {

            error("Invalid operand keyword!");

        } 
    } else if (nxt.Type == Token::symbol && nxt.Lexeme == "("){

        GetNextToken();

        //The result will be added to the stack
        expression();

        nxt = GetNextToken();

        if (!(nxt.Type == Token::symbol && nxt.Lexeme == ")")){
            error("Expecting ) after expression!");
        }

    } else {

        error("Expected operand!");

    }
}