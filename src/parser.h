#ifndef COMPPARSER_H
#define COMPPARSER_H

#include <string>
#include <vector>
#include "token.h"
#include "subroutine.h"
#include "symbollist.h"
#include "vmwriter.h"

class Parser {
    public:
        Parser(std::string fileName);
        std::vector<Subroutine> StartParse();
        Token GetNextToken();
        Token PeekNextToken();

        VmWriter output;

        //A list of known classes collected during previous 
        std::vector <SymbolList*> knownClasses;

        //Make a list of subroutines we have defined here
        std::vector <Subroutine> subRoutines;

        //Make a list of subroutines we should deine elswhere
        std::vector <Subroutine> speculativeSubroutines;

        std::vector <Subroutine> toBeFound;

        bool isExpression (Token t);        

        int lineNumber;

        SymbolList* doesClassExit(std::string className);

        bool isOperand(Token t);

        void classDeclar();

        void popVar(Symbol *s);
        void pushVar(Symbol *s);
        void constructString(std::string s);
        
    private:
        Subroutine* doSubRoutine(std::string part1, std::string part2, std::string retType, int noParams, Subroutine::SubType type);
        std::ifstream sourceStream;
        std::vector <std::string> workingList;
        bool unInitialisedFlag = false;
        int expressionNo;
        SymbolList *currentTable;
        std::string CurrentClassName;
        std::string expressionType;
        std::string workingType;
        std::string currentSubRetType;
        std::string classRef;
        Token currentToken;

        int labelCounter;

        void error(std::string msg);

        void memberDeclar();
        void classVarDeclar();
        void type();
        void subroutineDeclar();
        void paramList();
        void subroutineBody();
        void statement();
        void varDeclarStatement();
        void letStatemnt();
        void ifStatement();
        void whileStatement();
        void doStatement();
        void subroutineCall();
        void expressionList();
        void returnStatemnt();
        void expression();
        void relationalExpression();
        void ArithmeticExpression();
        void term();
        void factor();
        void operand();
};

#endif