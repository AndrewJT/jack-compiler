#include "parser.h"

//The following are a load of utility helper functions used by the parser

Parser::Parser(std::string fileName){

    sourceStream = std::ifstream(fileName);

    if (!sourceStream.good()) {
        std::cout << fileName + " does not exist! Or there was a reading error" << std::endl;
        //Error
        return;
    }

    size_t lastindex = fileName.find_last_of("."); 
    std::string rawname = fileName.substr(0, lastindex);

    this->output = VmWriter(rawname+ ".vm");

    this->lineNumber = 1;
    this->unInitialisedFlag = false;
}

std::vector<Subroutine> Parser::StartParse(){

    //Create a symbol table for the program. This is for classes
    SymbolList programTable;
    currentTable = &programTable;

    classDeclar();

    for(std::size_t i=0; i<speculativeSubroutines.size(); ++i){
        Subroutine *x = &speculativeSubroutines[i];
        for(std::size_t j=0; j<subRoutines.size(); ++j){
            Subroutine *y = &subRoutines[j];
            if(!(x->className == y->className && x->subName == y->subName && x->paramNo == y->paramNo)){
                //This subroutine hasnt been found yet add it to a list
                toBeFound.push_back(*x);
            }
        }
    }

    output.close();

    return toBeFound;
}

Token Parser::GetNextToken(){
    currentToken = Token::GetNextToken(&sourceStream, &lineNumber);
    //std::cout << "\n<Get> " << currentToken.toString();
    return currentToken;
}

Token Parser::PeekNextToken(){
    currentToken = Token::PeekNextToken(&sourceStream, &lineNumber);
    //std::cout << "\n(Peek)" << currentToken.toString();
    return currentToken;
}

void Parser::error(std::string msg){
    std::cout << "\n" << CurrentClassName <<" ERROR: Line " << this->lineNumber << ": " << msg;
    //std::cout << "\nFinal token: " << this->currentToken.lineNumber << ": " << this->currentToken.toString() << std::endl;
    exit(0);
}

SymbolList* Parser::doesClassExit(std::string className){
    //Loop through class list
    for(std::size_t i=0; i<knownClasses.size(); ++i){
        Symbol *s = knownClasses[i]->isInScope("className");
        if(s->Type == "class"){
            return knownClasses[i];
        }
    }

    return NULL;
}

//Helper as this is used a lot
//Pass it a token and it will decide if it could be the start of an expression
bool Parser::isExpression (Token t){
    switch (t.Type)
    {
        case Token::num:
            return true;
            break;
        case Token::id:
            return true;
            break;
        case Token::string_literal:
            return true;
            break;
        case Token::keyword:
            if(t.Lexeme == "true"){
                return true;
            } else if(t.Lexeme == "false"){
                return true;
            } else if(t.Lexeme == "null"){
                return true;
            } else if(t.Lexeme == "this"){
                return true;
            } else {
                return false;
            }
            break;
        case Token::symbol:
            if(t.Lexeme == "("){
                return true;
            }
            if(t.Lexeme == "-"){
                return true;
            }
            if(t.Lexeme == "~"){
                return true;
            }
            break;    
        default:
            return false;
            break;
    }

    return false;
}

bool Parser::isOperand(Token t){

    //There should also be a check for (expression) but that will cause a mind bending headache
    //But if there is an expression this should return true anyway
    //So it's all good probably

    switch (t.Type)
    {
        case Token::num:
            return true;
            break;
        case Token::id:
            return true;
            break;
        case Token::string_literal:
            return true;
            break;
        case Token::keyword:
            if(t.Lexeme == "true" || t.Lexeme == "false" || t.Lexeme == "null" || t.Lexeme == "this") {
                return true;
            }
            break;
        case Token::symbol:
            if(t.Lexeme == "("){   
                return true;
            }
            break;
        default:
            break;
    }

    return false;

}

//Feed subroutines here as Class.subRoutineName()
Subroutine* Parser::doSubRoutine(std::string className, std::string subName, std::string retType, int noParams, Subroutine::SubType type){
    //This subroutine type also has an arg for the method
    if(type == Subroutine::METHOD){
        noParams++;
    }

    output.writeCall(className + "." + subName, noParams);

    //Loop through known subroutines
    for(std::size_t i=0; i<subRoutines.size(); ++i){
        Subroutine *s = &subRoutines[i];

        if(s->className == className && s->subName == subName && s->paramNo == noParams){
            //Check return type
            if(retType == s->returnType || retType == "*"){
                return s;
            }
        }
    }

    //Dont think it does. Add to speculation
    speculativeSubroutines.push_back(Subroutine(subName, className, retType, type, noParams));

    return &speculativeSubroutines.back();

}

//Pop variable from the top of the stack to location of symbol
void Parser::popVar(Symbol *s){
    if(s->Kind == Symbol::STATIC){
        output.writePop(VmWriter::STATIC, s->number);
    } else if(s->Kind == Symbol::FIELD){
        output.writePop(VmWriter::THIS, s->number);
    } else if(s->Kind == Symbol::ARGUMENT){
        output.writePop(VmWriter::ARG, s->number);
    } else if(s->Kind == Symbol::VAR){
        output.writePop(VmWriter::LOCAL, s->number);
    } else {
        error("Trying to writer invalid variable type!");
    }
}

//Push variable at memory location to the top of the stack
void Parser::pushVar(Symbol *s){
    if(s->Kind == Symbol::STATIC){
        output.writePush(VmWriter::STATIC, s->number);
    } else if(s->Kind == Symbol::FIELD){
        output.writePush(VmWriter::THIS, s->number);
    } else if(s->Kind == Symbol::ARGUMENT){
        output.writePush(VmWriter::ARG, s->number);
    } else if(s->Kind == Symbol::VAR){
        output.writePush(VmWriter::LOCAL, s->number);
    } else {
        error("Trying to writer invalid variable type!");
    }
}

void Parser::constructString(std::string s){

    //New string takes length
    output.writePush(VmWriter::CONST, s.length());
    output.writeCall("String.new", 1);

    //Add all the characters
    for(int i = 0; i<s.length(); i++) {

        //Set i
        output.writePush(VmWriter::CONST, s[i]);

        //Add char to THAT string
        output.writeCall("String.appendChar", 2);

    }

}