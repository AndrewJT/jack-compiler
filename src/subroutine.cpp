#include <cstdarg>
#include <iostream>
#include <string>
#include "subroutine.h"

Subroutine::Subroutine(std::string name, std::string className, std::string returnType, SubType type){

    this->subName = name;
    this->className = className;
    this->returnType = returnType;
    this->paramNo = 0;
    this->type = type;

}

Subroutine::Subroutine(std::string name, std::string className, std::string returnType, SubType type, std::string * s, ...){

    this->subName = name;
    this->className = className;
    this->returnType = returnType;
    this->type = type;

    va_list params;
    va_start(params, s);
    int count = 1;

    while(s){
        paramTypes.push_back(*s);
        count ++;
    }

    this->paramNo = count;

}

void Subroutine::addType(std::string typeName){

    paramTypes.push_back(typeName);
    paramNo++;
}

//This might not work if there are no args
bool Subroutine::isCompatible(std::string *types, ...){

    va_list params;
    va_start(params, types);
    int count = 0;

    while(types){

        if(count >= paramNo ){
            return false;
        }

        if(paramTypes[count] != *types){
            return false;
        }
        count ++;
    }

    return true;

}

Subroutine::Subroutine(std::string name, std::string classorobjName, std::string retType, Subroutine::SubType type, int noParams){

    this->subName = name;
    this->className = classorobjName;
    this->returnType = retType;
    this->paramNo = noParams;
    this->type = type;

}

std::string Subroutine::toString(){

    std::string output = this->returnType + " " + this->className + "." + this->subName + "(";

    for(std::size_t i=0; i<paramTypes.size(); ++i){

        if(paramTypes.size() > 1 && i!=0){
            output+= ", ";
        }
        output += paramTypes[i];
    }

    output+= ") ";

    switch (type)
    {
    case SubType::CONSTRUCTOR:
        output+= "constructor";
        break;
    case SubType::METHOD:
        output+= "method";
        break;
    case SubType::FUNCTION:
        output+= "function";
        break;
    default:
        output+= "ERROR";
        break;
    }

    return output;

}

bool Subroutine::operator==(const Subroutine& rhs) const{

    return (subName == rhs.subName) && (className == rhs.className) && (type == rhs.type) && (returnType == rhs.returnType) && (paramNo == rhs.paramNo);

}