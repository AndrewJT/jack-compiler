#ifndef COMPSUBROUT_H
#define COMPSUBROUT_H

#include <string>
#include <vector>

class Subroutine {

    public:

        enum SubType {CONSTRUCTOR, METHOD, FUNCTION, UNKNOWN};

        std::string subName;
        std::string className;

        SubType type;

        std::string returnType;

        int paramNo;

        Subroutine(std::string name, std::string className, std::string returnType, SubType type, std::string * s, ...);
        Subroutine(std::string name, std::string className, std::string returnType, SubType type);
        
        //This is for unknown subroutines
        Subroutine(std::string name, std::string classorobjName, std::string retType, SubType type, int noParams);

        std::vector <std::string> paramTypes;

        bool isCompatible(std::string *types, ...);

        void addType(std::string typeName);

        std::string toString();

        bool operator==(const Subroutine& rhs) const;


};
#endif