#include "symbol.h"
#include "symbollist.h"
#include <string>


Symbol::Symbol(){
    this->Name = "";
    this->Type = "";

    this->Kind = Symbol::EMPTY;

    this->number = -1;
    this->Initialised = false;
    this->child = NULL;
    this->argCount = -1;
}

Symbol::Symbol(std::string name, std::string type, VarKind kind, int no){
    this->Name = name;
    this->Type = type;
    
    this->Kind = kind;

    this->number = no;
    this->Initialised = false;
    this->child = NULL;
    this->argCount = -1;
}

Symbol::Symbol(std::string name, std::string type, VarKind kind, int no, bool init){
    this->Name = name;
    this->Type = type;
    
    this->Kind = kind;

    this->number = no;
    this->Initialised = init;
    this->child = NULL;
    this->argCount = -1;
}

//For subroutines
Symbol::Symbol(std::string name, std::string type, VarKind kind, int no, SymbolList *child, int argNo){
    this->Name = name;
    this->Type = type;
    
    this->Kind = kind;

    this->number = no;
    this->Initialised = true;
    this->child = child;
    this->argCount = argNo;
}

Symbol::VarKind Symbol::KindFromString(std::string s){

    if(s == "static"){
        return Symbol::STATIC;
    } else if(s == "field"){
        return Symbol::FIELD;
    } else if(s == "argument"){
        return Symbol::ARGUMENT;
    } else if(s == "var"){
        return Symbol::VAR;
    } else if(s == "class"){
        return Symbol::CLASS;
    } else{
        return Symbol::EMPTY;
    }

}

bool Symbol::isPrimative(){
    if(this->Type == "boolean" || this->Type == "char" || this->Type == "int"){
        return true;
    }

    //This is some kind of compex object
    return false;
}