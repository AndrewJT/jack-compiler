#ifndef COMPSYMBOL_H
#define COMPSYMBOL_H

#include <string>
#include <vector>

class SymbolList;

class Symbol {

    public:
        //Ways a variable can be declared as 
        enum VarKind {STATIC, FIELD, ARGUMENT, VAR, CLASS, FUNCTION, CONSTRUCTOR, METHOD, EMPTY};

        //Identifier of variable
        std::string Name;

        //Variable type as string
        std::string Type;

        //Has variable been initialised with data?
        bool Initialised;

        //Way the variable was defined
        VarKind Kind;

        //Number for storage location
        int number;

        //For subroutines
        int argCount;

        //If this symbol has its own child use this
        SymbolList *child;

        Symbol();
        Symbol(std::string name, std::string type, VarKind kind, int no);
        Symbol(std::string name, std::string type, VarKind kind, int no, bool init);
        Symbol(std::string name, std::string type, VarKind kind, int no, SymbolList *child, int argNo);

        static VarKind KindFromString(std::string s);

        bool isPrimative();

    private:
        int x;

};

#endif