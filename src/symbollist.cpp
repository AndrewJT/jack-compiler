#include "symbollist.h"
#include <algorithm>
#include <iostream>

SymbolList::SymbolList(){
    this->parent = NULL;
    zeroNumbers();
}

SymbolList::SymbolList(SymbolList *parent){
    this->parent = parent;
    zeroNumbers();
}

void SymbolList::zeroNumbers(){

    for (unsigned int i = 0; i<9; i++){
        symbolNumbers[i] = 0;
    }
}

void SymbolList::debugPrint(int depth){

    if(depth < 1){
        return;
    }

    std::cout << "\n===== SYMBOL TABLE ====\n";
    std::vector<Symbol>::iterator i = symbols.begin();
    while (i != symbols.end()){

        std::cout << i->Kind << ": no." << i->number << " <" << i->Type << "> " << i->Name <<std::endl;
        i++;
    }

    //Try the parent
    if(this->parent != NULL){
        std::cout << "\n***PARENT***\n";
        this->parent->debugPrint(depth-1);
    }
}

Symbol* SymbolList::isInScope(std::string name){

    //debugPrint(5);

    //Look at our own list of symbols then query the parent
    //(If there is infact a parent)
    std::vector<Symbol>::iterator i = symbols.begin();
    while (i != symbols.end()){

        if(i->Name == name){
            return i.base();
        }
        i++;
    }

    //Try the parent
    if(this->parent != NULL){
        return this->parent->isInScope(name);
    }

    return NULL;

}

int SymbolList::getNextNumber(Symbol::VarKind kind){
    int number = symbolNumbers[kind];
    symbolNumbers[kind] ++;
    return number;
}

//Add an un-initialised variable to the symbol table
Symbol* SymbolList::AddSymbol(std::string name, std::string type, Symbol::VarKind kind){
    symbols.push_back(Symbol(name, type, kind, getNextNumber(kind)));
    return &symbols.back();
}

//Add an initialised variable to the symbol table
Symbol* SymbolList::AddSymbol(std::string name, std::string type, Symbol::VarKind kind, bool init){
    symbols.push_back(Symbol(name, type, kind, getNextNumber(kind), init));
    return &symbols.back();
}

//Ad sub routine
Symbol* SymbolList::AddSymbol(std::string name, std::string type, Symbol::VarKind kind, SymbolList *child, int argNo){
    symbols.push_back(Symbol(name, type, kind, getNextNumber(kind), child, argNo));

    return &symbols.back();
}

int SymbolList::numberOfFields(){

    return symbolNumbers[1];

}

int SymbolList::numberOfLocals(){
    return symbolNumbers[3];
}