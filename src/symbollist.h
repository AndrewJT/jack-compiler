#ifndef SYMBOLLIST_H
#define SYMBOLLIST_H

#include "symbol.h"
#include <vector>

class SymbolList {

    public:        

        SymbolList *parent;

        SymbolList();
        SymbolList(SymbolList *parent);

        Symbol *isInScope(std::string name);

        void debugPrint(int depth);

        int getNextNumber(Symbol::VarKind kind);

        Symbol* AddSymbol(std::string name, std::string type, Symbol::VarKind kind);

        Symbol* AddSymbol(std::string name, std::string type, Symbol::VarKind kind, bool init);

        //This is for subroutines
        Symbol* AddSymbol(std::string name, std::string type, Symbol::VarKind kind, SymbolList *child, int argNo);

        int numberOfFields();
        int numberOfLocals();

    private:
        int symbolNumbers[6];
        std::vector<Symbol>symbols;
        void zeroNumbers();
};

#endif