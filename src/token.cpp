#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include "token.h"

using namespace std;

Token::Token(){
  this->Lexeme = "EmptyToken";
  this->Type = Token::eof;
  this->lineNumber = 0;
}

Token::Token(std::string lexeme, TokenTypes type, int lineNo) {
  this->Lexeme = lexeme;
  this->Type = type;
  this->lineNumber = lineNo;
}

std::string Token::toString(){
  return "(" + std::to_string(lineNumber) + ")<" + typeToString(this->Type) + "> '" + this->Lexeme + "'";
}

Token Token::PeekNextToken(std::ifstream *source, int *currentLineNo){

  //Use the normal get method, then return the stream position to what it was before
  std::streampos startPos = source->tellg();
  int line = *currentLineNo;
  Token t = GetNextToken(source, currentLineNo);
  source->seekg(startPos);
  *currentLineNo = line;

  return t;

}

Token Token::GetNextToken(std::ifstream *source, int *currentLineNo) {

  char c;
  char nxt;
  TokeniserMode mode = normal;
  std::string word;

  if(source == NULL){
    cout << "Error NULL pointer to source!";
    exit(0);
  }

  if(!source->is_open()){
    cout << "Error opening file";
    exit(0);
  }

  if(!source->good()){  ///TODO Check this to avoid segfault
    //Error
    std::cout << "There was a problem reading the source file\n";

    exit(0);
  }

  while (source->get(c)) {

    if(c == '\n'){
      //Advance line no
      (*currentLineNo) ++;
    }

    nxt = source->peek();
    word += c;

    switch (mode) {
      case normal:
        if (c == '/') {
          // Could be the start of a comment
          //Peep the next one
          if(nxt == '/'){
            mode = comment;
          } else if (nxt == '*'){
            //Its a multi-line comment
            mode = multilinecomment;
          } else {
            //This / is a divide symbol
            return Token(word, symbol, *currentLineNo);
          }
        } else if (isalpha(c) || c == '_') {
          // This is a letter, is this the start of an keyword or id?
          mode = collectingkeyid;
          word = c;
        } else if (c == '"') {
          // The start of a string literal
          mode = collectingstringlit;
          word = "";
        } else if (isdigit(c)) {
          // Is this an integer number?
          mode = collectingint;
          word = c;
        } else if (c == '='){
          //Assignment operator
          return Token("=", symbol, *currentLineNo);
        } else if (c == ' ' || c == '\n' || c == '\t'){
          word = "";
        } else if (isSymbol(word)) {
          //find out if this is an other symbol
          return Token(string(1, c), symbol, *currentLineNo);
        }

        if (nxt == -1) {
          // Various EOF like signals
          return Token("EOF", eof, *currentLineNo);
        }
        break;

      case comment:
        if (c == '\n') {
          mode = normal;
          word = "";
        } else if (c == -1) {
          // EOF
          return Token("EOF", eof, *currentLineNo);
        }
        break;

      case multilinecomment:
        if (c == '*') {
          //This could be the end of the multi line comment
          if(nxt == '/'){
            //This is the end of the comment
            //COnsume the /
            source->get();
            mode = normal;
            word = "";
          }
        } else if (c == -1) {
          // EOF
          return Token("EOF", eof, *currentLineNo);
        }
        break;

      case collectingkeyid:
        // Check if the end of the word has been reached
        if (!isalnum(c) && c != '_') {
          // We have reached the end of the word
          source->seekg(-1, std::ios::cur);
          word = word.substr(0, word.size() - 1);
          if (isKeyword(word)) {
            return Token(word, keyword, *currentLineNo);
          } else {
            return Token(word, id, *currentLineNo);
          }
        } else if (c == -1){
          //This is an EOF char. Move the stream position back by 1
          //Then return the previous word
          source->seekg(-1, std::ios::cur);
          word = word.substr(0, word.size() - 1);
          return Token(word, keyword, *currentLineNo);
        }
        break;

      case collectingstringlit:
        // Keep scooping up this string literal until the end of it
        if (c == '"') {
          // This is the end
          // Chop off the end char
          word = word.substr(0, word.size() - 1);
          return Token(word, string_literal, *currentLineNo);
        } else if (c == -1) {
          //An EOF in the middle of a string is probs invalid
          return Token("EOF", eof, *currentLineNo);
        }
        break;

      case collectingint:
        if (!isdigit(c)) {
          // Chop off the end char
          word = word.substr(0, word.size() - 1);
          source->seekg(-1, std::ios::cur);
          return Token(word, num, *currentLineNo);
        } else if (c == -1){
          //Return the int and move buff pos back one
          source->seekg(-1, std::ios::cur);
          word = word.substr(0, word.size() - 1);
          return Token(word, num, *currentLineNo);
        }
        break;
    }
  }

  //An error
  std::cout << "An error occured\n" << mode;
  return Token("EOF", eof, *currentLineNo);
}

std::string Token::typeToString(Token::TokenTypes type){

  switch (type)
  {
    case keyword:
      return "keyword";
    case id:
      return "identifier";
    case string_literal:
      return "string_literal";
    case num:
      return "number";
    case eof:
      return "EOF";
    case symbol:
      return "Symbol";
    default:
      std::cout << "Error\n";
      return "Error";
  }
}



bool Token::isKeyword(std::string s) {

  static const std::string keywords[] = {"class",   "constructor", "method", "function", "int",
              "boolean", "char",        "void",   "var",      "static",
              "field",   "let",         "do",     "if",       "else",
              "while",   "return",      "true",   "false",    "null",
              "this"};

  if (std::find(std::begin(keywords), std::end(keywords), s) != std::end(keywords)) {
    return true;
  }
  return false;
}

bool Token::isSymbol(std::string c) {

  static const std::string symbols = "()[]{},;.+-*/&|~<>";

  if(symbols.find(c) != std::string::npos) {
    return true;
  } else {
    return false;
  }

}