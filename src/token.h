#ifndef COMPTOKENS_H
#define COMPTOKENS_H

#include <string>
#include <vector>

class Token {
    public:
      enum TokenTypes { keyword, id, string_literal, symbol, num, eof };
      TokenTypes Type;
      std::string Lexeme;

      std::string toString(); 

      static std::string typeToString(TokenTypes type);

      Token();
      Token(std::string lexeme, TokenTypes type, int lineNo);

      static Token GetNextToken(std::ifstream *source, int *currentLineNo);
      static Token PeekNextToken(std::ifstream *source, int *currentLineNo);

      int lineNumber;

    private:

        static bool isKeyword(std::string s);
        static bool isSymbol(std::string c);

        enum TokeniserMode {
            comment,
            multilinecomment,
            normal,
            collectingkeyid,
            collectingstringlit,
            collectingint
        };

        static std::vector<std::string> keywords;

};

#endif