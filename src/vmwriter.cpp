#include "vmwriter.h"

VmWriter::VmWriter(){
    //Do nothing?
}

VmWriter::VmWriter(std::string outputFile){
    output.open(outputFile);

    if(!output.good()){
        std::cout<< "Error with file opening";
    }
}

void VmWriter::writePush(Segment seg, int index){
    output << "push " << ToString(seg) << " " << index << std::endl;
}

void VmWriter::writePop(Segment seg, int index){
    output << "pop " << ToString(seg) << " " << index << std::endl;
}

void VmWriter::writeArithmetic(VmWriter::Command cmd){
    output << ToString(cmd) << std::endl;
}

void VmWriter::writeLabel(std::string label){
    output << "label " << label << std::endl;
}

void VmWriter::writeGoto(std::string label){
    output << "goto " << label << std::endl;
}

//Jump to label if top stack element is not 0
void VmWriter::writeIf(std::string label){
    output << "if-goto " << label << std::endl;
}

void VmWriter::writeCall(std::string label, int locals){
    output << "call " << label << " " << locals << std::endl;
}

void VmWriter::writeFunction(std::string label, int locals){
    output << "function " << label << " " << locals << std::endl;
}

void VmWriter::writeReturn(){
    output << "return" << std::endl;
}

void VmWriter::close(){
    output.close();
}

std::string VmWriter::ToString(VmWriter::Segment s){
    switch (s)
    {
    case VmWriter::CONST:
        return "constant";
        break;
    case VmWriter::ARG:
        return "argument";
        break;
    case VmWriter::LOCAL:
        return "local";
        break;
    case VmWriter::STATIC:
        return "static";
        break;
    case VmWriter::THIS:
        return "this";
        break;
    case VmWriter::THAT:
        return "that";
        break;
    case VmWriter::POINTER:
        return "pointer";
        break;
    case VmWriter::TEMP:
        return "temp";
        break;    
    default:
        std::cout << "\nINVALID SEGMENT TYPE\n";
        exit(0);
        break;
    }
}

void VmWriter::debugMsg(std::string msg){
    //output << "label DEBUG:" << msg << std::endl;
}

std::string VmWriter::ToString(VmWriter::Command s){
    switch (s)
    {
    case VmWriter::ADD:
        return "add";
        break;
    case VmWriter::SUB:
        return "sub";
        break;
    case VmWriter::NEG:
        return "neg";
        break;
    case VmWriter::EQ:
        return "eq";
        break;
    case VmWriter::GT:
        return "gt";
        break;
    case VmWriter::LT:
        return "lt";
        break;
    case VmWriter::AND:
        return "and";
        break;
    case VmWriter::OR:
        return "or";
        break;  
    case VmWriter::NOT:
        return "not";
        break;    
    default:
        std::cout << "\nINVALID SEGMENT TYPE\n";
        exit(0);
        break;
    }
}