#ifndef COMPVMWRITER_H
#define COMPVMWRITER_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

class VmWriter {
    public:

        VmWriter();
        VmWriter(std::string outputFile);

        std::ofstream output;

        enum Segment{CONST, ARG, LOCAL, STATIC, THIS, THAT, POINTER, TEMP};
        enum Command{ADD, SUB, NEG, EQ, GT, LT, AND, OR, NOT};

        static std::string ToString(Segment s);
        static std::string ToString(Command c);

        void writePush(Segment seg, int index);

        void writePop(Segment seg, int index);

        void writeArithmetic(Command cmd);

        void writeLabel(std::string label);

        void writeGoto(std::string label);

        void writeIf(std::string label);

        void writeCall(std::string label, int locals);
        
        void writeFunction(std::string label, int locals);

        void writeReturn();

        void close();

        void debugMsg(std::string msg);
};

#endif